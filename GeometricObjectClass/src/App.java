import com.devcamp.Circle;
import com.devcamp.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(2.7);
        Rectangle rectangle = new Rectangle(4.5, 6.5);
        System.out.println(circle.toString());
        System.out.println(rectangle.toString());
        System.out.println("Dien tich hinh tron: " + circle.getArea());
        System.out.println("Chi vi hinh tron: " + circle.getPerimeter());
        System.out.println("Dien Tich HCN: " + rectangle.getArea());
        System.out.println("Chu vi HCN: " + rectangle.getPerimeter());

    }
}
