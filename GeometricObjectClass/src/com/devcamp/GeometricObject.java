package com.devcamp;

public interface GeometricObject {
    public double getArea();

    public double getPerimeter();

}
