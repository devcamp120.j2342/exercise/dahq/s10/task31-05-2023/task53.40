package com.devcamp;

public class Rectangle implements GeometricObject {
    private double length;
    private double width;

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.length * this.width;
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2 * (this.length + this.width);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public String toString() {
        return "Rectangle [length=" + length + ", width=" + width + "]";
    }

}
